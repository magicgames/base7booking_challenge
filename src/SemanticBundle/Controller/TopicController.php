<?php

namespace SemanticBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\SerializationContext;

/**
 * Topic controller.
 *
 * @Route("topic")
 */
class TopicController extends Controller
{
    /**
     * Lists all topic entities.
     *
     * @Route("/", name="topic_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $uploaderForm =   $form = $this->createForm('SemanticBundle\Form\UploadTopicType');
        // replace this example code with whatever you need
        return $this->render('topic/index.html.twig', array('page'=>'topic',
            'uploaderForm'=> $uploaderForm->createView(),
        ));
    }

    /**
     * Lists all topic entities.
     *
     * @Route("/list", name="topic_list")
     * @Method("GET")
     */
    public function listParentAction(Request $request)
    {
        $limit = $request->get('rows', 10);
        $page = $request->get('page', 1);
        $topics =  $this->get('semantic.service.topic_crud')->listTopic();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $topics,
            $page,
            $limit,
            array()
        );
        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($pagination, 'json');

        return new Response($response);
    }

    /**
     * Edit a Topic entities.
     *
     * @Route("/upload_topic", name="upload_topic")
     * @Method({"GET","POST"})
     */
    public function uploadTopicAction(Request $request)
    {
        $topicService = $this->get('semantic.service.import_topic');
        $uploaderForm = $this->createForm('SemanticBundle\Form\UploadTopicType');
        $uploaderForm->handleRequest($request);

        if ($uploaderForm->isValid() && $uploaderForm->isSubmitted()) {

            try {
                $raw_file = $uploaderForm['file']->getData();
                if ($raw_file == null) {
                    throw new \Exception('File does not exist');
                }
                $topicService->importFromCsv($raw_file);
                $this->addFlash('success','File Uploaded correctly');
            } catch (\Exception $e)
            {
                $this->addFlash('danger',$e->getMessage());
            }


        } else {
            $this->addFlash('danger', 'Error in form');

        }
        return $this->redirectToRoute('topic_index');
    }


    /**
     * Edit a Topic entities.
     *
     * @Route("/list/edit", name="topic_edit_ajax")
     * @Method({"GET","POST"})
     */
    public function EditParentAction(Request $request)
    {
        $result = $this->get('semantic.service.topic_crud')->processTopic();
        $serializer = $this->container->get('jms_serializer');
        $jsonContent = $serializer
            ->serialize(
                $result,
                'json',
                SerializationContext::create()
                    ->enableMaxDepthChecks()
            );

        return new Response($jsonContent);
    }

    /**
     * Lists all  Alternate topic entities.
     *
     * @Route("/list/{parent_id}", name="topic_alternate", requirements={"parent_id": "\d+"})
     * @Method("GET")
     */
    public function listAlternateAction(Request $request, $parent_id)
    {
        $limit = $request->get('rows', 10);
        $page = $request->get('page', 1);
        $topics = $this->get('semantic.service.topic_crud')->listAlternate($parent_id);
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $topics,
            $page,
            $limit,
            array()
        );
        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($pagination, 'json');

        return new Response($response);
    }

    /**
     * Edit Alternate Topic.
     *
     * @Route("/list/{id_parent}/edit", name="topic_edit_alternate_ajax",requirements={"id": "\d+"})
     * @Method({"GET","POST"})
     */

    public function EditAlternateAction(Request $request, $id_parent)
    {
        $result = $this->get('semantic.service.topic_crud')->processAlternateTopic($id_parent);
        $serializer = $this->container->get('jms_serializer');
        $jsonContent = $serializer
            ->serialize(
                $result,
                'json',
                SerializationContext::create()
                    ->enableMaxDepthChecks()
            );

        return new Response($jsonContent);
    }
}
