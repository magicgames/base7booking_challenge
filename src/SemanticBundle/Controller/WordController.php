<?php

namespace SemanticBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\SerializationContext;

/**
 * Word controller.
 *
 * @Route("word")
 */
class WordController extends Controller
{
    /**
     * @Route("/", name="word_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $uploaderForm = $form = $this->createForm('SemanticBundle\Form\UploadWordType');

        // replace this example code with whatever you need
        return $this->render(
            'word/index.html.twig',
            array(
                'page' => 'word',
                'uploaderForm' => $uploaderForm->createView(),
            )
        );
    }

    /**
     * Lists all topic entities.
     *
     * @Route("/list", name="word_list")
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        $limit = $request->get('rows', 10);
        $page = $request->get('page', 1);
        $words = $this->get('semantic.service.word_crud')->listWord();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $words,
            $page,
            $limit,
            array()
        );
        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($pagination, 'json');

        return new Response($response);
    }

    /**
     * Edit a Topic entities.
     *
     * @Route("/edit", name="word_edit")
     * @Method({"GET","POST"})
     */
    public function editAction(Request $request)
    {
        $process_topic = $this->get('semantic.service.word_crud');
        $result = $process_topic->processWord();
        $serializer = $this->container->get('jms_serializer');
        $jsonContent = $serializer
            ->serialize(
                $result,
                'json',
                SerializationContext::create()
                    ->enableMaxDepthChecks()
            );

        return new Response($jsonContent);
    }

    /**
     * Upload a Word entities.
     *
     * @Route("/upload", name="upload_word")
     * @Method({"GET","POST"})
     */
    public function uploadAction(Request $request)
    {
        $wordService = $this->get('semantic.service.import_word');
        $uploaderForm = $this->createForm('SemanticBundle\Form\UploadWordType');
        $uploaderForm->handleRequest($request);
        if ($uploaderForm->isValid() && $uploaderForm->isSubmitted()) {
            try {

                $raw_file = $uploaderForm['file']->getData();
                if ($raw_file === null) {
                    throw new \Exception('File does not exist');
                }
                $wordService->importFromCsv($raw_file);
                $this->addFlash('success', 'File Uploaded correctly');

            } catch (\Exception $e) {
                $this->addFlash('danger', $e->getMessage());
            }

        } else {
            $this->addFlash('danger', 'Error in form');

        }

        return $this->redirectToRoute('word_index');


    }
}
