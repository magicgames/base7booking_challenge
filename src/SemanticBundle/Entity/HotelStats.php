<?php

namespace SemanticBundle\Entity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * HotelStats
 *
 * @ORM\Table(name="hotel_stats")
 * @ORM\Entity(repositoryClass="SemanticBundle\Repository\HotelStatsRepository")
 */
class HotelStats
{

    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="totalReviews", type="integer")
     */
    private $totalReviews = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="positiveReviews", type="integer")
     */
    private $positiveReviews = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="negativeReviews", type="integer")
     */
    private $negativeReviews = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="positiveScore", type="decimal", precision=10, scale=2)
     */
    private $positiveScore = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="negativeScore", type="decimal", precision=10, scale=2)
     */
    private $negativeScore = 0;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set totalReviews
     *
     * @param integer $totalReviews
     *
     * @return HotelStats
     */
    public function setTotalReviews($totalReviews)
    {
        $this->totalReviews = $totalReviews;

        return $this;
    }

    /**
     * Get totalReviews
     *
     * @return int
     */
    public function getTotalReviews()
    {
        return $this->totalReviews;
    }

    /**
     * Set positiveReviews
     *
     * @param integer $positiveReviews
     *
     * @return HotelStats
     */
    public function setPositiveReviews($positiveReviews)
    {
        $this->positiveReviews = $positiveReviews;

        return $this;
    }

    /**
     * Get positiveReviews
     *
     * @return int
     */
    public function getPositiveReviews()
    {
        return $this->positiveReviews;
    }

    /**
     * Set negativeReviews
     *
     * @param integer $negativeReviews
     *
     * @return HotelStats
     */
    public function setNegativeReviews($negativeReviews)
    {
        $this->negativeReviews = $negativeReviews;

        return $this;
    }

    /**
     * Get negativeReviews
     *
     * @return int
     */
    public function getNegativeReviews()
    {
        return $this->negativeReviews;
    }

    /**
     * Set positiveScore
     *
     * @param string $positiveScore
     *
     * @return HotelStats
     */
    public function setPositiveScore($positiveScore)
    {
        $this->positiveScore = $positiveScore;

        return $this;
    }

    /**
     * Get positiveScore
     *
     * @return string
     */
    public function getPositiveScore()
    {
        return $this->positiveScore;
    }

    /**
     * Set negativeScore
     *
     * @param string $negativeScore
     *
     * @return HotelStats
     */
    public function setNegativeScore($negativeScore)
    {
        $this->negativeScore = $negativeScore;

        return $this;
    }

    /**
     * Get negativeScore
     *
     * @return string
     */
    public function getNegativeScore()
    {
        return $this->negativeScore;
    }
}
