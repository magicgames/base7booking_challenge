<?php

namespace SemanticBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * ReviewTopicResult
 * @JMS\ExclusionPolicy("none")
 * @ORM\Table(name="review_topic_result",
 *   uniqueConstraints={@ORM\UniqueConstraint(columns={"topic_id", "review_id"})})
 * @ORM\Entity(repositoryClass="SemanticBundle\Repository\ReviewTopicResultRepository")
 */
class ReviewTopicResult
{

    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Topic", inversedBy="topicResult")
     * @ORM\JoinColumn(name="topic_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $topic;

    /**
     * @ORM\ManyToOne(targetEntity="Review", inversedBy="reviewTopicResult")
     * @ORM\JoinColumn(name="review_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $review;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set topic
     *
     * @param Topic $topic
     *
     * @return ReviewTopicResult
     */
    public function setTopic(Topic $topic = null)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return \SemanticBundle\Entity\Topic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set review
     *
     * @param Review $review
     *
     * @return ReviewTopicResult
     */
    public function setReview(Review $review = null)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return \SemanticBundle\Entity\Review
     */
    public function getReview()
    {
        return $this->review;
    }
}
