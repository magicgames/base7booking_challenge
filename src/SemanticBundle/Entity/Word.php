<?php

namespace SemanticBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as JMS;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Word
 * @JMS\ExclusionPolicy("none")
 * @ORM\Table(name="word",
 *     indexes={@ORM\Index(name="score_idx", columns={"score"})},
 *     uniqueConstraints={@ORM\UniqueConstraint(columns={"name"})})
 * @ORM\Entity(repositoryClass="SemanticBundle\Repository\WordRepository")
 * @UniqueEntity(fields={"name"})
 */
class Word
{

    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="score", type="integer")
     * @Assert\Type(
     *     type="integer",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $score;


    /**
     * @var bool
     *
     * @ORM\Column(name="is_auto_populated", type="boolean",options={"default":false})
     */
    private $isAutoPopulated = false;

    /**
     * @ORM\OneToMany(targetEntity="ReviewWordResult", mappedBy="word",cascade={"persist", "remove"})
     * @JMS\Exclude
     */
    private $wordResult;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->wordResult = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Word
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return Word
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set isAutoPopulated
     *
     * @param boolean $isAutoPopulated
     *
     * @return Word
     */
    public function setIsAutoPopulated($isAutoPopulated)
    {
        $this->isAutoPopulated = $isAutoPopulated;

        return $this;
    }

    /**
     * Get isAutoPopulated
     *
     * @return boolean
     */
    public function getIsAutoPopulated()
    {
        return $this->isAutoPopulated;
    }

    /**
     * Add reviewWordResult
     *
     * @param ReviewWordResult $wordResult
     *
     * @return Word
     */
    public function addReviewWordResult(ReviewWordResult $wordResult)
    {
        $this->wordResult[] = $wordResult;

        return $this;
    }

    /**
     * Remove reviewWordResult
     *
     * @param ReviewWordResult $wordResult
     */
    public function removeReviewWordResult(ReviewWordResult $wordResult)
    {
        $this->wordResult->removeElement($wordResult);
    }

    /**
     * Get reviewWordResult
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviewWordResult()
    {
        return $this->wordResult;
    }
}
