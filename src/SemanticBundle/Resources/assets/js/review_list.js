( function ($) {
        'use strict';
        var review_list = {
            init: function () {
                var grid = $('#review-grid');

                var addOptions;
                var editOptions;
                var deleteOptions;
                var searchOptions;

                searchOptions = {

                    searchCaption: "Search Topic",
                    searchtext: "Search Topic",
                    closeOnEscape: true,
                    closeAfterSearch: true,
                    width: "500",
                    reloadAfterSubmit: true
                };
                deleteOptions = {
                    deleteCaption: "delete Post",
                    deletetext: "Delete Post",
                    closeOnEscape: true,
                    closeAfterEdit: true,
                    width: "500",
                    reloadAfterSubmit: true,
                    bottominfo: "Fields marked with (*) are required",
                    top: "60",
                    left: "5",
                    right: "5",
                    onclickSubmit: function (response, postdata) {

                    }
                };
                addOptions = {
                    /* Add */
                    addCaption: "Add Topic",
                    addtext: "Add",
                    closeOnEscape: true,
                    closeAfterAdd: true,
                    width: 500,
                    bottominfo: "Fields marked with (*) are required",

                    onclickSubmit: function (response, postdata) {

                    },
                    afterSubmit: function (response) {

                    }
                };
                editOptions = {
                    /* Edit */
                    reloadAfterSubmit: true,
                    closeOnEscape: true,
                    closeAfterEdit: true,
                    width: 500,
                    bottominfo: "Fields marked with (*) are required",

                    onclickSubmit: function (response, postdata) {

                    },
                    afterSubmit: function (response) {

                    }
                };

                grid.jqGrid({
                    datatype: 'json',
                    url: app.reviewList,
                    editurl: app.reviewList + 'edit',

                    colNames: ['id', 'hotel', 'score', 'review', 'word', 'topic'],
                    colModel: [
                        {name: 'id', width: 10},
                        {
                            name: 'hotel',
                            index: 'hotel',
                            width: 20,
                            search: false,
                            formatter: formatHotel,
                            formoptions: {
                                label: 'Hotel id*'
                            },
                            editrules: {
                                edithidden: true,
                                number: true,
                                required: true
                            }
                            ,
                            editable: true
                        },
                        {
                            name: 'score',
                            index: 'score',
                            width: 30,
                            search: false
                        },
                        {
                            name: 'review',
                            index: 'review',
                            formatter: textFormater,
                            editrules: {
                                edithidden: true,
                                text: true,
                                required: true
                            }
                            ,
                            editable: true
                        },
                        {
                            name: 'review_word_result',
                            index: 'review_word_result',
                            formatter: objectWordFormater,
                            width: 120,
                            search: false
                        }
                        ,
                        {
                            name: 'review_topic_result',
                            index: 'review_topic_result',
                            formatter: objectTopicFormater,
                            width: 40,
                            search: false
                        }

                    ],
                    caption: "Reviews",
                    gridview: true,
                    autowidth: true,
                    shrinkToFit: true,
                    viewrecords: true,
                    toppager: true,
                    rowList: [1, 5, 10, 20, "10000:All"],
                    rowNum: 10,
                    guiStyle: "bootstrap",
                    iconSet: "fontAwesome",
                    multiselect: false,
                    sortname: 'id',
                    pager: '#pager',
                    sortorder: "desc",
                    afterSubmit: function (response, postdata) {
                        var data = JSON.parse(response.responseText);
                        if (data.status === 'OK') {
                            //for successfull editing return true.
                            return [true, "Success"]; //  return [success,message,new_id]
                        } else {
                            return [false, data.message];
                        }
                    }

                    ,
                    rowattr: function (rd) {
                        var apply_css = '';
                        if (parseInt(rd.score) < 0) {
                            apply_css = 'bg-danger';
                        } else if (parseInt(rd.score) > 0) {
                            apply_css = 'bg-success';
                        } else {
                            apply_css = '';
                        }
                        return {"class": apply_css};
                    }
                    ,


                })
                ;

                grid.jqGrid("navGrid", {
                        edit: false,
                        add: true,
                        del: true,
                        search: true,
                        refresh: true,
                        view: true
                    },
                    editOptions,
                    addOptions,
                    deleteOptions,
                    searchOptions,
                    {closeOnEscape: true})
                    .jqGrid("inlineNav")
                    .jqGrid("filterToolbar")
                    .jqGrid("gridResize");


                function textFormater(cellvalue, options, rowObject) {
                    return "<span class='wrap'>" + cellvalue + "</span>";
                }
                function formatHotel(cellvalue, options, rowObject) {
                    return "<span class='wrap'>" + cellvalue.id + "</span>";
                }



                function objectTopicFormater(cellvalue, options, rowObject) {
                    var elements = '';
                    for (var i = 0, l = cellvalue.length; i < l; i++) {
                        var obj = cellvalue[i].topic;
                        elements += "<tr><td>" + obj.name + "</td></tr>";
                    }

                    return "<table class='table'><tr><th>topic</th></tr>" + elements + "</table>";
                }

                function objectWordFormater(cellvalue, options, rowObject) {
                    var elements = '';
                    for (var i = 0, l = cellvalue.length; i < l; i++) {
                        var obj = cellvalue[i].word;

                        elements += "<tr><td>" + obj.name + "</td><td>" + obj.score + "</td></tr>";
                    }

                    return "<table class='table'><tr><th>word</th><th>score</th></tr>" + elements + "</table>";
                }


                $('#process_reviews').on('click', function (e) {
                    e.preventDefault();
                    processReview(app.processReview);

                });

                $('#process_full_reviews').on('click', function (e) {
                    e.preventDefault();
                    processReview(app.processFullReview);

                });


                var processReview = function (url) {

                    var request = $.ajax({
                        url: url,
                        type: "post"
                    });
                    request.done(function (response, textStatus, jqXHR) {

                        grid.trigger('reloadGrid');
                        $('#myModal').modal();
                    });

                    // Callback handler that will be called on failure
                    request.fail(function (jqXHR, textStatus, errorThrown) {
                        // Log the error to the console
                        $('.error_message').html(textStatus);
                    });

                }


            }
        };


        $(document).ready(function () {
            review_list.init();
        })
    }(jQuery)
);
