<?php
/**
 * Created by PhpStorm.
 * User: ballbrk
 * Date: 30/05/2017
 * Time: 14:48
 */

namespace SemanticBundle\Services;

use SemanticBundle\Entity\Topic;
use Doctrine\ORM\EntityRepository;
use Ddeboer\DataImport\Reader\CsvReader;
use SplFileObject;
use Symfony\Component\HttpFoundation\File\File;
use SemanticBundle\Utils\TextSanitize;
use SemanticBundle\Interfaces\CsvImportInterface;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Security\Acl\Exception\Exception;

class ImportTopicService implements CsvImportInterface
{
    /**
     * @var EntityRepository
     */
    private $topicRepository;

    /**
     * @var TextSanitize
     */
    private $textSanitize;

    /**
     * ImportTopicService constructor.
     * @param EntityRepository $topicRepository
     * @param TextSanitize $textSanitize
     */
    public function __construct(EntityRepository $topicRepository, TextSanitize $textSanitize)
    {
        $this->topicRepository = $topicRepository;
        $this->textSanitize = $textSanitize;
    }

    /**
     * @param SplFileInfo $file
     */
    public function importFromCsv($file)
    {

            $csv_file = new SplFileObject($file->getPathName());
            $reader = new CsvReader($csv_file);
            $reader->setHeaderRowNumber(0);
            $reader->setStrict(false);
            $topic = null;
            foreach ($reader as $row) {
                if (array_key_exists('Topic', $row) and $row['Topic'] != '') {
                    $name = mb_strtolower(trim($this->textSanitize->sanitize($row['Topic'])));
                    $criteria = array('name' => $name);
                    $topic = $this->topicRepository->findOneOrCreate($criteria);

                }
                if (array_key_exists('Alternate Name', $row) and $row['Alternate Name'] != '') {
                    if ($topic) {
                        $name = mb_strtolower(trim($this->textSanitize->sanitize($row['Alternate Name'])));
                        $criteria = array(
                            'name' => $name,
                            'parentTopic' => $topic['entity'],
                        );
                        $this->topicRepository->findOneOrCreate($criteria);
                    }
                }
            }

    }
}
