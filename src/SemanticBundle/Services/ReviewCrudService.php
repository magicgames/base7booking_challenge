<?php

namespace SemanticBundle\Services;

use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityRepository;
use SemanticBundle\Utils\TextSanitize;
use Symfony\Component\Security\Acl\Exception\Exception;

class ReviewCrudService
{
    /**
     * @var \SemanticBundle\Repository\ReviewRepository
     */
    private $reviewRepository;

    /**
     * @var \SemanticBundle\Repository\HotelStatsRepository
     */
    private $hotelStatsRepository;

    protected $requestStack;

    protected $textSanitize;

    public function __construct(
        RequestStack $requestStack,
        EntityRepository $reviewRepository,
        EntityRepository $hotelStatsRepository,
        TextSanitize $textSanitize
    ) {
        $this->requestStack = $requestStack;
        $this->reviewRepository = $reviewRepository;
        $this->hotelStatsRepository= $hotelStatsRepository;
        $this->textSanitize = $textSanitize;
    }


    public function listReview()
    {
        $request = $this->requestStack->getCurrentRequest();
        $request = $request->query;


        $search = $request->get('_search', "false");
        $ord = $request->get('sord', 'asc');
        $field = $request->get('sidx', 'id');
        $field = ($field !== '') ? $field : 'id';
        $words = $this->getResults($request,$search,$field,$ord);

        return $words;
    }

    public function processReview()
    {
        $request = $this->requestStack->getCurrentRequest();
        $request = $request->request;
        $oper = $request->get("oper");
        $id = $request->get('id');
        $hotelId = $request->get('hotel');
        $review = $request->get('review');

        if (is_numeric($id)) {

            $criteria = array('id'=>$id,'review'=>$review);
        } else {
            $criteria = array('review'=>$review);
            
        }
        try {
            switch ($oper) {
                case 'add':
                    if ($review=='') {
                        throw new Exception("Review can't be null");
                    } else {
                        $hotel = $this->hotelStatsRepository->findOneOrCreate(array('id'=>$hotelId));
                        $criteria['hotel']= $hotel['entity'];
                        $topic = $this->reviewRepository->findOneOrCreate($criteria);
                        $result['result']=$topic;
                    }

                    break;
                case 'del':
                    $topic = $this->reviewRepository->find($id);
                    $this->reviewRepository->remove($topic, true);
                    break;


            }
            $result['status'] = 'OK';
        } catch (\Exception $e) {
            $result['status'] = 'KO';
            $result['message'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param  \Symfony\Component\HttpFoundation\Request $request
     * @param string $search
     * @param string $field
     * @param string $ord
     * @return array|mixed
     */
    private function getResults($request,$search,$field,$ord) {

        if ($search == "true") {
            $searchField = $request->get('searchField', false);
            $searchString = $request->get('searchString', false);
            $searchOper = $request->get('searchOper', false);
            $words = $this->reviewRepository->searchWord(
                $searchField,
                $searchString,
                $searchOper,
                $field,
                $ord
            );
        } else {
            $words = $this->reviewRepository->findAllSort($field, $ord);
        }
        return $words;

    }

}
