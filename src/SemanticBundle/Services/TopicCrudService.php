<?php

namespace SemanticBundle\Services;

use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityRepository;
use SemanticBundle\Utils\TextSanitize;
use Symfony\Component\Security\Acl\Exception\Exception;

class TopicCrudService
{
    /**
     * @var EntityRepository
     */
    private $topicRepository;

    protected $requestStack;

    protected $textSanitize;

    public function __construct(
        RequestStack $requestStack,
        EntityRepository $topicRepository,
        TextSanitize $textSanitize
    ) {
        $this->requestStack = $requestStack;
        $this->topicRepository = $topicRepository;
        $this->textSanitize = $textSanitize;
    }

    public function listTopic()
    {
        $request = $this->requestStack->getCurrentRequest();

        $request = $request->query;


        $search = $request->get('_search', "false");
        $ord = $request->get('sord', 'asc');
        $field = $request->get('sidx', 'id');
        $field = ($field !== '') ? $field : 'id';

        if ($search == "true") {
            $search_field = $request->get('searchField', false);
            $search_string = $request->get('searchString', false);
            $search_oper = $request->get('searchOper', false);
            $topics = $this->topicRepository->searchParentTopic(
                $search_field,
                $search_string,
                $search_oper,
                $field,
                $ord
            );
        } else {
            $topics = $this->topicRepository->prepareTopics($field, $ord);
        }

        return $topics;
    }

    public function listAlternate($parentId)
    {
        $request = $this->requestStack->getCurrentRequest();
        $request = $request->query;
        $search = $request->get('_search', "false");
        $ord = $request->get('sord', 'asc');
        $field = $request->get('sidx', 'id');
        $field = ($field !== '') ? $field : 'id';
        if ($search == "true") {
            $search_field = $request->get('searchField', false);
            $search_string = $request->get('searchString', false);
            $search_oper = $request->get('searchOper', false);
            $topics = $this->topicRepository->searchAltenateParentTopic(
                $search_field,
                $search_string,
                $search_oper,
                $field,
                $ord,
                $parentId
            );
        } else {
            $topics = $this->topicRepository->findAlternateByParent($parentId, $field, $ord);
        }

        return $topics;
    }

    public function processTopic()
    {
        $request = $this->requestStack->getCurrentRequest();
        $data = $request->request;
        $oper = $data->get("oper");
        $name = trim(mb_strtolower($this->textSanitize->sanitize($data->get("name"))));
        $id = $data->get('id');

        if (is_numeric($id)) {
            $criteria = array('name' => $name, 'id' => $id);

        } else {

            $criteria = array('name' => $name);
        }
        try {

            switch ($oper) {
                case 'add':
                    if ($name == "") {
                        throw new Exception('Name can not be null');
                    } else {
                        $criteria = array('name' => $name);
                        $topic = $this->topicRepository->findOneOrCreate($criteria);
                        $result['result'] = $topic;
                    }


                    break;
                case 'del':
                    $topic = $this->topicRepository->find($id);
                    $this->topicRepository->remove($topic, true);
                    break;
                case 'edit':
                    if ($name == "") {
                        throw new Exception('Name can not be null');
                    } else {

                        $topic = $this->topicRepository->update($criteria);
                        $result['result'] = $topic;
                    }

                    break;
            }
            $result['status'] = 'OK';
        } catch (\Exception $e) {
            $result['status'] = 'KO';
            $result['message'] = $e->getMessage();
        }

        return $result;
    }

    public function processAlternateTopic($idParent)
    {
        $request = $this->requestStack->getCurrentRequest();
        $data = $request->request;
        $oper = $data->get("oper");
        $name = $data->get("name");
        $id = $data->get('id');

        try {
            $parentTopic = $this->topicRepository->find($idParent);
            if (!$parentTopic) {
                throw new \Exception('Parent don\'t exists');
            } else {
                if (is_numeric($id)) {
                    $criteria = array('name' => $name, 'id' => $id, 'parentTopic' => $parentTopic);
                } else {
                    $criteria = array('name' => $name, 'parentTopic' => $parentTopic);
                }
                switch ($oper) {
                    case 'add':
                        $topic = $this->topicRepository->findOneOrCreate($criteria);
                        $result['result'] = $topic;
                        break;
                    case 'del':
                        $topic = $this->topicRepository->find($id);
                        $this->topicRepository->remove($topic, true);
                        break;

                    case 'edit':
                        $topic = $this->topicRepository->update($criteria);
                        $result['result'] = $topic;
                        break;
                }
                $result['status'] = 'OK';
            }

        } catch (\Exception $e) {
            $result['status'] = 'KO';
            $result['message'] = $e->getMessage();
        }

        return $result;
    }
}
