<?php


namespace SemanticBundle\Tests\Repository;

use PHPUnit\Framework\TestCase;
use SemanticBundle\Entity\HotelStats;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class HotelStatsRepositoryTest extends KernelTestCase
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }


    public function testUpdate()
    {

        $criteria = array('name' => 'test');
        $hotelStatsRepository = $this->em->getRepository('SemanticBundle:HotelStats');
        # $hotelStatsRepository->findOneOrCreate($criteria);
        $hotel = new HotelStats();
        $hotel->setId('20');
        $hotelStatsRepository->updateIfNew($hotel, true);


    }


    /**
     * @expectedException \Exception
     */
    public function testFindOne()
    {
        $criteria = array('name' => 'test');
        $hotelStatsRepository = $this->em->getRepository('SemanticBundle:HotelStats');
        $hotelStatsRepository->findOneOrCreate($criteria);

    }


    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}

