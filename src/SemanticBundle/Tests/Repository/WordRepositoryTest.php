<?php


namespace SemanticBundle\Tests\Repository;

use PHPUnit\Framework\TestCase;
use SemanticBundle\Entity\Word;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class WordRepositoryTest extends KernelTestCase
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * @expectedException \Exception
     */
    public function testSetScore()
    {
        $wordRepository = $this->em ->getRepository('SemanticBundle:Word');
        $criteria= array('score'=>'aa','name'=>'kkkkkk');
        $wordRepository->findOneOrCreate($criteria);
    }


    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}
