<?php


namespace SemanticBundle\Tests\Services;

use PHPUnit\Framework\TestCase;
use SemanticBundle\Services\ProcessReviewService;
use SemanticBundle\Utils\TextSanitize;
use SemanticBundle\Services\SemanticProcessService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProcessReviewTest extends KernelTestCase
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }


    public function testExtract()
    {
        $wordRepository =  $this->em ->getRepository('SemanticBundle:Word');
        $topicRepository = $this->em ->getRepository('SemanticBundle:Topic');
        $reviewRepository = $this->em ->getRepository('SemanticBundle:Review');
        $reviewTopicRepository = $this->em ->getRepository('SemanticBundle:ReviewTopicResult');
        $reviewWordRepository = $this->em ->getRepository('SemanticBundle:ReviewWordResult');
        $hotelStatsRepository = $this->em ->getRepository('SemanticBundle:HotelStats');
        $textSanitize = new TextSanitize();
        $semanticProcess = new SemanticProcessService($textSanitize);
        $service = new ProcessReviewService(
            $wordRepository,
            $topicRepository,
            $reviewRepository,
            $reviewTopicRepository,
            $reviewWordRepository,
            $hotelStatsRepository,
            $semanticProcess
        );
        $result = $service->checkReview();
        $this->assertEquals("OK", $result['status']);
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}
