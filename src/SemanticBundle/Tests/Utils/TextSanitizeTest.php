<?php
/**
 * Created by PhpStorm.
 * User: ballbrk
 * Date: 31/05/2017
 * Time: 13:35
 */

namespace SemanticBundle\Tests\Utils;

use PHPUnit\Framework\TestCase;
use SemanticBundle\Utils\TextSanitize;

class TextSanitizeTest extends TestCase
{
    public function testSanitize()
    {
        $sanitize = new TextSanitize();
        $result = $sanitize->sanitize("hola que   ()&&&    tal");

        $this->assertEquals("hola que tal", $result);
    }
}
